package br.com.fiap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages="br.com.fiap")
@SpringBootApplication
@EnableJpaRepositories
public class HomeMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeMonitorApplication.class, args);
	}
}
