package br.com.fiap.interfaces;

 

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.fiap.entity.Medicao; 

public interface MedicaoDao  extends CrudRepository<Medicao, Long> {
  

    @Query("from Medicao m order by m.dataMedicao desc ")
    public List<Medicao> consuntaUltimasMedicoes(Pageable pageable);
    

    @Query("from Medicao m where m.agua = 0.00 order by m.dataMedicao desc ")
    public List<Medicao> consuntaUltimaMedicaoInicial(Pageable pageable);
    

    @Query("from Medicao m where m.dataMedicao between ?1 and ?2 order by m.dataMedicao asc ")
    public List<Medicao> consuntaPorPeriodo(Date periodoInicio, Date periodoFinal);
    
}
