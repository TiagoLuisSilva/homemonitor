package br.com.fiap.interfaces;

 

import org.springframework.data.repository.CrudRepository;

import br.com.fiap.entity.MedicaoConsolidado; 

public interface MedicaoConsolidadoDao  extends CrudRepository<MedicaoConsolidado, Long> {
  
 
    
}
