package br.com.fiap.dto;

import java.util.Date;

public class ConsultaConsumoDTO {

	private String dataInicio;
	private String dataFinal;
	
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	
}
