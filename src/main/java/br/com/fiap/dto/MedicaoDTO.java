package br.com.fiap.dto;

import java.util.Date;

import br.com.fiap.entity.Medicao;

public class MedicaoDTO {


	 private Long id;	  
	 private Date dataMedicao; 
	 private Double agua; 	  
	 private Double energia;
     private MedicaoDTO() {} 
	 public MedicaoDTO(Medicao medicao){
		 this.id = medicao.getId();
		 this.dataMedicao = medicao.getDataMedicao();
		 this.agua = medicao.getAgua();
		 this.energia = medicao.getEnergia();
	 }
	 public Medicao  toMedicao(){
		 Medicao medicao = new Medicao();
		 medicao.setId(this.id);
		 medicao.setDataMedicao(this.dataMedicao);
		 if (medicao.getId() == null){
			 medicao.setDataMedicao(new Date());
		 }
		 medicao.setAgua(this.agua);
		 medicao.setEnergia(this.energia);
		 
		 return medicao;
	 }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDataMedicao() {
		return dataMedicao;
	}
	public void setDataMedicao(Date dataMedicao) {
		this.dataMedicao = dataMedicao;
	}
	public Double getAgua() {
		return agua;
	}
	public void setAgua(Double agua) {
		this.agua = agua;
	}
	public Double getEnergia() {
		return energia;
	}
	public void setEnergia(Double energia) {
		this.energia = energia;
	}
	
}
