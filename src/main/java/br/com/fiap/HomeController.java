package br.com.fiap;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.dto.ConsultaConsumoDTO;
import br.com.fiap.dto.MedicaoDTO;
import br.com.fiap.entity.Medicao;
import br.com.fiap.entity.MedicaoConsolidado;
import br.com.fiap.interfaces.MedicaoConsolidadoDao;
import br.com.fiap.interfaces.MedicaoDao;

@RestController
public class HomeController {

   @Autowired
   private MedicaoDao medicaoDao;
   @Autowired
   private MedicaoConsolidadoDao medicaoConsolidadoDao;
   
    @RequestMapping("/consumo")
    public ResponseEntity<HomeVO> consumo() {
    	HomeVO home = new HomeVO();
    	home.setAguaCubicos(getRandom());
    	home.setCorrente(getRandom());
        return new ResponseEntity<HomeVO>(home, HttpStatus.OK);
    }

    @RequestMapping("/grafico")
    public ResponseEntity<Object > testeGrafico() {
    	Calendar data = Calendar.getInstance();
    	
    	Date datass = new Date();
    	List<Object> lista = new ArrayList<Object>();
    	Object[] val = {datass,getRandom()};
    	Object[] val2 = {datass,getRandom()};
    	lista.add(val);
    	lista.add(val2);
        return new ResponseEntity<Object>(lista, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/consumosemagua", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> recebeConsumoSemAgua(@RequestBody MedicaoDTO medicaoDTO) {
    	Medicao medicao  = medicaoDTO.toMedicao();
 
    	Pageable top  = new PageRequest(0, 2);
    	List<Medicao> lista=   medicaoDao.consuntaUltimasMedicoes(top);
    	boolean anteriorComValor = false;
    	boolean primeiroComValor = false;
    	if ( lista != null  && !lista.isEmpty()){
    		Medicao m1 = lista.get(0);
    		if (m1.getAgua() != 0D){
    			primeiroComValor = true;
	    		for (Medicao med : lista){ 
	        		if (med.getAgua() != 0D){
	        			anteriorComValor = true;
	        		}
	    		} 
    		}
    	}  
        if (medicao.getAgua() == 0D && anteriorComValor){ 
        	if (primeiroComValor){  
	            top  = new PageRequest(0, 1); 
	        	lista = medicaoDao.consuntaUltimaMedicaoInicial(top);
	
	        	if ( lista != null  && !lista.isEmpty()){
	        		Medicao m = lista.get(0);
	    			List<Medicao>  listaConsumo = medicaoDao.consuntaPorPeriodo(m.getDataMedicao(), new Date());
	        		// calculo consumo no meriodo
	        		MedicaoConsolidado medicaoConsolidado  = new MedicaoConsolidado();
	        		medicaoConsolidado.setAgua(0.0D);
	        		medicaoConsolidado.setEnergia(0.0D);
	        		for (Medicao med : listaConsumo){
	        			medicaoConsolidado.setAgua(medicaoConsolidado.getAgua() + med.getAgua());
	        			medicaoConsolidado.setEnergia(medicaoConsolidado.getEnergia() + med.getEnergia());
	        		}
	        		medicaoConsolidadoDao.save(medicaoConsolidado);
	        	}

	        	medicaoDao.save(medicao); 
        	}
        }
    	return new ResponseEntity<Object>("POST OK", null, HttpStatus.OK); 
    }
    
    @RequestMapping(value = "/api/consumo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> recebeConsumo(@RequestBody MedicaoDTO medicaoDTO) {
    	Medicao medicao  = medicaoDTO.toMedicao();
 
        if (medicao.getAgua() != 0D){
        	medicaoDao.save(medicao);  
        }  
    	return new ResponseEntity<Object>("POST OK", null, HttpStatus.OK); 
    }
    
    @RequestMapping(value = "/api/consulta", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> consultaConsumo(@RequestBody ConsultaConsumoDTO filtro) throws ParseException {
    	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    	Date dataInicioFiltro = (Date)formatter.parse(filtro.getDataInicio());
    	Date dataFinalFiltro = (Date)formatter.parse(filtro.getDataFinal());
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(dataFinalFiltro);
    	calendar.set(Calendar.HOUR, 23);
    	calendar.set(Calendar.MINUTE, 59);
    	calendar.set(Calendar.SECOND, 59);
    	dataFinalFiltro = calendar.getTime();
    	List<Medicao> lista = medicaoDao.consuntaPorPeriodo(dataInicioFiltro, dataFinalFiltro); 
    	List<Object> listaRetorno = new ArrayList<Object>();
    	Date dataInicio = null;
    	Date dataFinal = null;
    	Double totalAgua = 0D;
    	Double totalEnergia = 0D;
    	int cont = 1;
    	for (Medicao medicao : lista){
    		
    		if (medicao.getAgua() != 0.00D){
    			if (cont == 1){
    				dataInicio = medicao.getDataMedicao();
    			} else {
    				dataFinal = medicao.getDataMedicao();
    			}
    			totalAgua = totalAgua + medicao.getAgua();
    			totalEnergia = totalEnergia + medicao.getEnergia(); 
    			cont ++;
    		} else {    
    			if (totalAgua != 0.00D){
    				formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    				if (dataFinal == null){
    					dataFinal = new Date();
    				}
    				String datas = formatter.format(dataInicio) + " a " +   formatter.format(dataFinal);
	    	    	Object[] val = {datas, totalAgua, totalEnergia};    	  
	    	    	listaRetorno.add(val);
	    	    	
	    	      	totalAgua = 0.00D;
	    	    	totalEnergia = 0D;
	    	    	dataInicio=null;
	    	    	dataFinal=null;
	    			cont = 1;
    			}
    		} 
    	}
     
        return new ResponseEntity<Object>(listaRetorno, HttpStatus.OK); 
    }
    
    
    public  BigDecimal getRandom(){ 
    	float minX = 50.0f;
    	float maxX = 100.0f;

    	Random rand = new Random();

    	float finalX = rand.nextFloat() * (maxX - minX) + minX;
            BigDecimal randFromDouble = BigDecimal.valueOf(finalX);
            return randFromDouble;
    }
}
