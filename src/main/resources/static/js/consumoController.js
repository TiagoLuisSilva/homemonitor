app.controller('ConsumoController', function($scope, $http, SERVER) {
 
	$scope.consultar = function() {
		$http.get(SERVER.url+ '/consumo').success(function(data){
			$scope.valor = data;
			$scope.energia = data.corrente;
			$scope.agua = data.aguaCubicos;
		});
	};
	$scope.consultar();
});